#pragma once

#include <map>

class MapHekker
{
    public:
        MapHekker(const std::map<int, int> &map);

        std::map<int, int> firstHekk();
        std::map<int, int> secondHekk();
        std::map<int, int> thirdHekk();

    protected:
        virtual int generateValue();

    protected:
        std::map<int, int> map;
};