#!/usr/bin/perl

{
	
package MyWebServer;

use warnings;
use strict;
use HTTP::Server::Simple::CGI;
use base qw(HTTP::Server::Simple::CGI);
use CGI;
use CGI::Header;
use Scalar::Util qw(looks_like_number);

my %dispatch = (
	'/status' => \&resp_status,
	# ...
);

sub handle_request {
	my $self = shift;
	my $cgi  = shift;

	my $path = $cgi->path_info();
	my $handler = $dispatch{$path};

	if (ref($handler) eq "CODE") {
		print "HTTP/1.0 200 OK\r\n";
		$handler->($cgi);
	} else {
		print "HTTP/1.0 404 Not found\r\n";
		print $cgi->header,
			  $cgi->start_html('Not found'),
			  $cgi->h1('Not found'),
			  $cgi->end_html;
	}
}

sub resp_status {
	my $cgi  = shift;
	return if !ref $cgi;

	print "Content-Type: application/json\r\n\r\n",
		  "{\"status\": \"running\"}\r\n";
}

} 

# start the server on port 8080
my $pid = MyWebServer->new(8080)->run();

