#include <iostream>
#include <string>

#include "numericals.h"

int main(int argc, char *argv[])
{
    // TODO: error handling and testing

    if (std::string(argv[1]) == "-p") {
        std::cout << isPrime(std::stoi(argv[2])) << std::endl;
    }

    if (std::string(argv[1]) == "-f") {
        std::cout << factorial(std::stoi(argv[2])) << std::endl;
    }

    // TODO: check of number is even and testing

    return 0;
}
