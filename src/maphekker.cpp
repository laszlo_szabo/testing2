#include "maphekker.h"

#include <algorithm>
#include <ctime>

MapHekker::MapHekker(const std::map<int, int> &map)
    : map(map)
{
    srand(time(0));
}

int MapHekker::generateValue()
{
    return rand() % 10;
}

std::map<int, int> MapHekker::firstHekk()
{
    std::map<int, int> hekkd = map;

    for (auto key : map) {
        hekkd[key.first] = generateValue();
    }

    return hekkd;
}

std::map<int, int> MapHekker::secondHekk()
{
    std::map<int, int> hekkd = map;

    for (auto key : map) {
        hekkd[key.first] = generateValue() * 2;
    }

    return hekkd;
}

std::map<int, int> MapHekker::thirdHekk()
{
    std::map<int, int> hekkd = map;

    for (auto key : map) {
        hekkd[key.first] = generateValue() * 3;
    }

    return hekkd;
}
