#include <limits.h>
#include "../../src/numericals.h"
#include "gtest/gtest.h"

TEST(FactorialTest, Negative) {
    EXPECT_EQ(1, factorial(-5));
    EXPECT_EQ(1, factorial(-1));
    EXPECT_GT(factorial(-10), 0);
}

TEST(FactorialTest, Zero) {
    EXPECT_EQ(1, factorial(0));
}

TEST(FactorialTest, Positive) {
    EXPECT_EQ(1, factorial(1));
    EXPECT_EQ(2, factorial(2));
    EXPECT_EQ(6, factorial(3));
    EXPECT_EQ(40320, factorial(8));
}

TEST(IsPrimeTest, Negative) {
    EXPECT_FALSE(isPrime(-1));
    EXPECT_FALSE(isPrime(-2));
    EXPECT_FALSE(isPrime(INT_MIN));
}

TEST(IsPrimeTest, Trivial) {
    EXPECT_FALSE(isPrime(0));
    EXPECT_FALSE(isPrime(1));
    EXPECT_TRUE(isPrime(2));
    EXPECT_TRUE(isPrime(3));
}

TEST(IsPrimeTest, Positive) {
    EXPECT_FALSE(isPrime(4));
    EXPECT_TRUE(isPrime(5));
    EXPECT_FALSE(isPrime(6));
    EXPECT_TRUE(isPrime(23));
}
