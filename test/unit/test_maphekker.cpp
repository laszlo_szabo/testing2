#include "maphekker.h"
#include "gtest/gtest.h"

class MapHekkerTest : public testing::Test
{
	public:
		MapHekkerTest() 
			: originalMap({ {0, 0} }),
			  hekker(new MapHekker(originalMap))
		{}
    protected:
        virtual void SetUp()
        {
        }

        virtual void TearDown()
        {
        }

    protected:
        std::map<int, int> originalMap;
        MapHekker *hekker = nullptr;
};

TEST_F(MapHekkerTest, TestGivesValue)
{
    ASSERT_NE(originalMap, hekker->firstHekk());
}

////////////////////////////////
// PARAMETERIZED TESTS
////////////////////////////////

class ParameterizedMapHekkerTest : public testing::TestWithParam<std::map<int, int>>
{

};

std::vector<std::map<int, int>> maps = {
        { {0, 0} }
};

INSTANTIATE_TEST_CASE_P(ParamMapTest, ParameterizedMapHekkerTest, testing::ValuesIn(maps));

TEST_P(ParameterizedMapHekkerTest, testFirstHekk)
{
    MapHekker hekker(GetParam());
}