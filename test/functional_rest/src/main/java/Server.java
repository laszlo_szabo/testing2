import java.io.BufferedReader;
import java.io.InputStreamReader;

class Server {

    Process serverProcess;

    public void start() {
        try {
            serverProcess = Runtime.getRuntime().exec("perl " + System.getProperty("server.command"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        serverProcess.destroy();
    }
}
