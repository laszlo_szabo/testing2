import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import static com.jayway.restassured.RestAssured.when;

public class TestServer {

  Server server;

  @Before
  public void initialize() {
    server = new Server();
    server.start();
  }

  @After
  public void teardown() {
    server.stop();
  }

  @Test
  public void testIsAlive() {
    assertThat(server.serverProcess.isAlive(), is(equalTo(true)));
  }

  @Test
  public void testStatus() {
    when()
      .get("http://localhost:8080/status")
    .then()
      .statusCode(200)
      .body("status", equalTo("running"));
  }
}
