import java.io.BufferedReader;
import java.io.InputStreamReader;

class Num {

    Num(String option, int value) {
        this.option = option;
        this.value = value;
    }

    public String runCommand() {
        try {
            Process p = Runtime.getRuntime().exec(System.getProperty("num.command") + " " + option + " " + value);
            p.waitFor();

            return new BufferedReader(new InputStreamReader(p.getInputStream())).readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private String option;
    private int value;
}
